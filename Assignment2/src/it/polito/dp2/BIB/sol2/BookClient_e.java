package it.polito.dp2.BIB.sol2;

import it.polito.dp2.BIB.sol2.jaxb.crossref.Items;
import it.polito.dp2.rest.gbooks.client.MyErrorHandler;
import it.polito.dp2.xml.biblio.PrintableItem;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class BookClient_e {

	JAXBContext jcGB;
	javax.xml.validation.Validator validatorGB;

	JAXBContext jcCR;
	javax.xml.validation.Validator validatorCR;

	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage: java BookClient N keyword1 keyword2 ...");
			System.exit(1);
		}

		if (!isInt(args[0])) {
			System.err.println("Usage: java BookClient NUMBER keyword1 keyword2 ...");
			System.exit(1);
		}

		if(Integer.valueOf(args[0]) < 1) {
			System.err.println("Usage: java BookClient [N >= 1] keyword1 keyword2 ...");
			System.exit(1);
		}

		try {
			BookClient_e bclient = new BookClient_e();

			/* Google Books API search */
			bclient.PerformSearchGB(Integer.parseInt(args[0]), args);
			System.out.println("*******************************************************************************");
			/* CrossRef API search */
			bclient.PerformSearchCR(Integer.parseInt(args[0]), args);
		} catch (Exception ex) {
			System.err.println("Error during execution of operation");
			ex.printStackTrace(System.out);
		}
	}

	public BookClient_e() throws Exception {

		/* GB: create validator that uses the DataTypes schema */
		SchemaFactory sfGB = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
		Schema schemaGB = sfGB.newSchema(new File("xsd/gbooks/DataTypes.xsd"));
		validatorGB = schemaGB.newValidator();
		validatorGB.setErrorHandler(new MyErrorHandler());

		/* GB: create JAXB context related to the classed generated from the DataTypes schema */
		jcGB = JAXBContext.newInstance("it.polito.dp2.BIB.sol2.jaxb.gbooks");

		/*
		 * **********************************************************************************
		 */

		/* CR: create validator that uses the DataTypesCR schema */
		SchemaFactory sfCR = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
		Schema schemaCR = sfCR.newSchema(new File("xsd/gbooks/DataTypesCR.xsd"));
		validatorCR = schemaCR.newValidator();
		validatorCR.setErrorHandler(new MyErrorHandler());

		/* CR: create JAXB context related to the classed generated from the DataTypes schema */
		jcCR = JAXBContext.newInstance("it.polito.dp2.BIB.sol2.jaxb.crossref");

	}

	public void PerformSearchGB(Integer n, String[] kw) {
		final int BPP = 40;

		/* build the JAX-RS client object */
		Client client = ClientBuilder.newClient();

		/* create empty list */
		List<PrintableItem> pitems = new ArrayList<PrintableItem>();

		/* build the web target */
		WebTarget target = client.target(getBaseURIGB()).path("volumes");

		/*
		 * perform a get request using mediaType=APPLICATION_JSON and convert
		 * the response into a SearchResult object
		 */
		StringBuffer queryString = new StringBuffer(kw[1]);
		for (int i = 2; i < kw.length; i++) {
			queryString.append(' ');
			queryString.append(kw[i]);
		}
		//System.out.println("Searching " + queryString + " on Google Books:");
		int cc = 0, j = 0;
		it.polito.dp2.BIB.sol2.jaxb.gbooks.SearchResult result;

		do {
			Response response = target
									.queryParam("q", queryString)
									.queryParam("printType", "books")
									.queryParam("maxResults", BPP)
									.queryParam("startIndex", cc * BPP)
									.request()
									.accept(MediaType.APPLICATION_JSON)
									.get();
			if (response.getStatus() != 200) {
				System.out.println("Error in remote operation: " + response.getStatus() + " " + response.getStatusInfo());
				return;
			}

			response.bufferEntity();
			result = response.readEntity(it.polito.dp2.BIB.sol2.jaxb.gbooks.SearchResult.class);

			for (it.polito.dp2.BIB.sol2.jaxb.gbooks.Items item : result.getItems()) {
				try {
					/* validate item */
					JAXBSource source = new JAXBSource(jcGB, item);
					System.out.println("Validating " + item.getSelfLink());
					validatorGB.validate(source);
					System.out.println("Validation OK, adding item to list");

					/* add item to list */
					pitems.add(Factory.createPrintableItem(BigInteger.valueOf(j++), item.getVolumeInfo()));
					if (j == n)
						break;
				} catch (org.xml.sax.SAXException se) {
					System.out.println("Validation Failed");
					/* print error messages */
					Throwable t = se;
					while (t != null) {
						String message = t.getMessage();
						if (message != null)
							System.out.println(message);
						t = t.getCause();
					}
				} catch (IOException e) {
					System.out.println("Unexpected I/O Exception");
				} catch (JAXBException e) {
					System.out.println("Unexpected JAXB Exception");
				}
			}
			cc++;
		} while (pitems.size() < n && result.getTotalItems().subtract(BigInteger.valueOf(cc * BPP)).signum() == 1);

		System.out.println("Validated GBooks Bibliography items: " + pitems.size());
		for (PrintableItem item : pitems)
			item.print();
		System.out.println("End of Validated Bibliography items");

	}

	public void PerformSearchCR(Integer n, String[] kw) {
		final int BPP = 100;

		/* build the JAX-RS client object */
		Client client = ClientBuilder.newClient();

		/* create empty list */
		List<PrintableItem> pitems = new ArrayList<PrintableItem>();

		/* build the web target */
		WebTarget target = client.target(getBaseURICR()).path("types").path("book").path("works");

		/*
		 * build concat of keywords in order to perform search
		 */
		StringBuffer queryString = new StringBuffer(kw[1]);
		for (int i = 2; i < kw.length; i++) {
			queryString.append('+');
			queryString.append(kw[i]);
		}

		int cc = 0, j = 0;
		it.polito.dp2.BIB.sol2.jaxb.crossref.SearchResult result;

		do {
			Response response = target
									.queryParam("query.bibliographic", queryString)
									.queryParam("rows", BPP)
									.queryParam("offset", BPP * cc)
									.request()
									.accept(MediaType.APPLICATION_JSON)
									.get();

			if (response.getStatus() != 200) {
				System.out.println("Error in remote operation: " + response.getStatus() + " " + response.getStatusInfo());
				return;
			}
			response.bufferEntity();
			result = response.readEntity(it.polito.dp2.BIB.sol2.jaxb.crossref.SearchResult.class);

			for (Items item : result.getMessage().getItems()) {
				try {
					/* validate item */
					JAXBSource source = new JAXBSource(jcCR, item);
					System.out.println("Validating " + item.getTitle());
					validatorCR.validate(source);
					System.out.println("Validation OK, adding item to list");

					/* add item to list */
					pitems.add(Factory.createPrintableItem(BigInteger.valueOf(j++), item));
					if (j == n)
						break;
				} catch (org.xml.sax.SAXException se) {
					System.out.println("Validation Failed");
					/* print error messages */
					Throwable t = se;
					while (t != null) {
						String message = t.getMessage();
						if (message != null)
							System.out.println(message);
						t = t.getCause();
					}
				} catch (IOException e) {
					System.out.println("Unexpected I/O Exception");
				} catch (JAXBException e) {
					System.out.println("Unexpected JAXB Exception");
				}
			}
			cc++;
		} while (pitems.size() < n && result.getMessage().getTotalResults().subtract(BigInteger.valueOf(cc * BPP)).signum() == 1);
		
		System.out.println("Validated CrossRef Bibliography items: " + pitems.size());
		for (PrintableItem item : pitems)
			item.print();
		System.out.println("End of Validated Bibliography items");
	}

	private static URI getBaseURIGB() {
		return UriBuilder.fromUri("https://www.googleapis.com/books/v1").build();
	}

	private static URI getBaseURICR() {
		return UriBuilder.fromUri("http://api.crossref.org").build();
	}

	private static boolean isInt(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
