package it.polito.dp2.BIB.sol2;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import it.polito.dp2.BIB.sol2.jaxb.neo4j.Data;
import it.polito.dp2.BIB.sol2.jaxb.neo4j.Node;
import it.polito.dp2.BIB.sol2.jaxb.neo4j.ObjectFactory;
import it.polito.dp2.BIB.sol2.jaxb.neo4j.Relation;
import it.polito.dp2.BIB.sol2.jaxb.neo4j.RelationshipType;
import it.polito.dp2.BIB.sol2.jaxb.neo4j.Traverse;
import it.polito.dp2.rest.neo4j.client.Neo4jClientException;

public class MyNeo4jClient {
	
	Client client;
	WebTarget target;
	String uri = "http://localhost:7474/db";
	String urlProperty = "it.polito.dp2.BIB.ass2.URL";
	private ObjectFactory of = new ObjectFactory();

	public MyNeo4jClient() throws Neo4jClientException {
		try {
			client = ClientBuilder.newClient();

			String customUri = System.getProperty(urlProperty);
			if (customUri != null)
				uri = customUri;
			target = client.target(uri).path("data");
		} catch (Exception e) {
			throw new Neo4jClientException(e);
		}
	}
	
	public void close() {
		client.close();
	}

	public Node createNode(String title) throws Neo4jClientException {
		Data data = of.createData();
		
		/* setting object properties in order to perform request */
		data.setTitle(title);
		
		try {
			Node node = target.path("node")
				  .request(MediaType.APPLICATION_JSON_TYPE)
				  .post(Entity.json(data), Node.class);
			return node;
		} catch (WebApplicationException | ProcessingException e) {
			throw new Neo4jClientException(e);
		}
	}
	
	/* build relations */
	public void createRelation(Node from, Node to) throws Neo4jClientException {
		Relation r = of.createRelation();
		
		/* setting object properties in order to perform request */
		r.setType("CitedBy");
		r.setTo(to.getSelf());
		
		try {
			 client.target(from.getSelf()).path("relationships")
			 	.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(r));
		} catch (WebApplicationException | ProcessingException e) {
			throw new Neo4jClientException(e);
		}
		
	}
	
	/* get relations from node within max depth */
	public Set<Node> getCitedBy(Node from, int maxDepth) throws Neo4jClientException {
		RelationshipType rt = of.createRelationshipType();
		Traverse t = of.createTraverse();		
		
		/* setting objects properties */
		rt.setDirection("out");
		rt.setType("CitedBy");
		t.setMaxDepth(BigInteger.valueOf(maxDepth));
		t.setRelationships(rt);
		
		try {
			List<Node> nodes = client.target(from.getSelf()).path("traverse").path("node")
									.request(MediaType.APPLICATION_JSON_TYPE)
									.post(Entity.json(t), new GenericType<List<Node>>() {});
			return nodes.stream().collect(Collectors.toSet());
		} catch (WebApplicationException | ProcessingException e) {
			throw new Neo4jClientException(e);
		}
	
	}
}
