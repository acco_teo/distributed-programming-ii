package it.polito.dp2.BIB.sol2;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.BIB.sol2.jaxb.crossref.Items;
import it.polito.dp2.BIB.sol2.jaxb.gbooks.IndustryIdentifier;
import it.polito.dp2.BIB.sol2.jaxb.gbooks.VolumeInfo;
import it.polito.dp2.xml.biblio.PrintableItem;
import it.polito.pad.dp2.biblio.BiblioItemType;
import it.polito.pad.dp2.biblio.BookType;



public class Factory extends it.polito.dp2.xml.biblio.Factory {

	public static PrintableItem createPrintableItem(BigInteger id, VolumeInfo info) {
		BiblioItemType item = new BiblioItemType();
		item.setId(id);
		item.setTitle(info.getTitle());
		item.setSubtitle(info.getSubtitle());
		item.getAuthor().addAll(info.getAuthors());
		BookType book = new BookType();
		book.setPublisher(info.getPublisher());
		book.setYear(info.getPublishedDate());
		List<IndustryIdentifier> list = info.getIndustryIdentifiers();
		IndustryIdentifier ii = list.get(0);
		if (ii!=null)
			book.setISBN(ii.getIdentifier());
		item.setBook(book);
		return createPrintableItem(item);
	}
	
	public static PrintableItem createPrintableItem(BigInteger id, Items item) {
		BiblioItemType bit = new BiblioItemType();
		bit.setId(id);
		bit.setTitle(item.getTitle().get(0));
		bit.setSubtitle(item.getSubtitle().isEmpty() ? null : item.getSubtitle().get(0));
		bit.getAuthor()
				.addAll(item.getAuthor().stream()
						.map(a -> a.getGiven() != null ? a.getGiven() + " " + a.getFamily() : a.getFamily())
						.collect(Collectors.toSet()));
		BookType b = new BookType();
		b.setPublisher(item.getPublisher());
		XMLGregorianCalendar d;
		try {
			d = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			d.clear();
			d.setYear(Integer.valueOf(item.getIssued().getDateParts().get(0).toString().subSequence(0, 4).toString()));
			b.setYear(d);
		} catch (DatatypeConfigurationException e) {
			b.setYear(null);
		}
		b.setISBN(item.getISBN().get(0));
		bit.setBook(b);
		return createPrintableItem(bit);
	}

}
