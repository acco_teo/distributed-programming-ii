package it.polito.dp2.BIB.sol3.db;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class solBookshelf {
	
	private String name;
	private Set<BigInteger> itemIds;
	private AtomicInteger readsCount;
	
	public solBookshelf(String name) {
		this.name = name;
		ConcurrentHashMap<BigInteger, Integer> tmp = new ConcurrentHashMap<>();
		this.itemIds = tmp.newKeySet();
		this.readsCount = new AtomicInteger(0);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addId(BigInteger id) {
		this.itemIds.add(id);
	}
	
	public void removeId(BigInteger id) {
		this.itemIds.remove(id);
	}
	
	public Set<BigInteger> getItemIds() {
		return this.itemIds;
	}
	
	public BigInteger incrementReadsCount() {
		return BigInteger.valueOf(readsCount.incrementAndGet());
	}
	

}
