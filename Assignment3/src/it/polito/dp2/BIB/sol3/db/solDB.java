package it.polito.dp2.BIB.sol3.db;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class solDB {
	
	private static final solDB db = new solDB();
	private static Map<BigInteger, solBookshelf> bookshelves;
	private static BigInteger id;
	
	private solDB() {
		bookshelves = new ConcurrentHashMap<>();
		id = BigInteger.ZERO;
	}
	
	public static solDB getInstance() {
		return db;
	}
	
	synchronized public BigInteger createBookshelf(String name) {
		BigInteger currentId = id;
		id = id.add(BigInteger.ONE);
		bookshelves.put(currentId, new solBookshelf(name));
		return currentId;
	}
	
	public Map<BigInteger, String> getBookshelves(String name) {
		Map<BigInteger, String> returnMap = new HashMap<>();
		/* iterating over snapshots of the list is not affected by concurrent add operations, 
		 * allowing concurrently adding and iterating
		 */
		for(Entry<BigInteger, solBookshelf> e : bookshelves.entrySet()) {
			if(e.getValue().getName().contains(name))
				returnMap.put(e.getKey(), e.getValue().getName());
		}
		
		return returnMap;
	}
	
	public Set<BigInteger> getItemsFromBookshelf(BigInteger id) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return null;
		return b.getItemIds();
		
	}
	
	public boolean deleteBookshelf(BigInteger id) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return false;
		bookshelves.remove(id);
		return true;
	}
	
	synchronized public BigInteger addItemToBookshelf(BigInteger id, BigInteger iid) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return BigInteger.valueOf(-1);
		if(b.getItemIds().size() == 20) {
			return BigInteger.valueOf(-2);
		}
		b.addId(iid);
		return iid;

	}
	
	public BigInteger removeItemFromBookshelf(BigInteger id, BigInteger iid) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return BigInteger.valueOf(-1);
		b.removeId(iid);
		return iid;
	}
	
	public BigInteger getReadsNumber(BigInteger id) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return BigInteger.valueOf(-1);
		return b.incrementReadsCount();
	}

	public void cascadeDeleteFromBookshelf(BigInteger id) {
		for(solBookshelf b : bookshelves.values()) {
			b.removeId(id);
		}
	}

	public void incrementReadsCount(BigInteger id) {
		solBookshelf b = bookshelves.get(id);
		if(b == null)
			return;
		b.incrementReadsCount();
	}
		
}
