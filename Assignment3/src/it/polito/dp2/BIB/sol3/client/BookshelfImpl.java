package it.polito.dp2.BIB.sol3.client;
import java.util.HashSet;
import java.util.Set;
import java.math.BigInteger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.polito.dp2.BIB.ass3.DestroyedBookshelfException;
import it.polito.dp2.BIB.ass3.ItemReader;
import it.polito.dp2.BIB.ass3.ServiceException;
import it.polito.dp2.BIB.ass3.TooManyItemsException;
import it.polito.dp2.BIB.ass3.UnknownItemException;

public class BookshelfImpl implements it.polito.dp2.BIB.ass3.Bookshelf {
	
	private boolean destroyed;
	private Client client;
	private String name;
	private String self;
	private String reads;
	
	public BookshelfImpl(String name, String selfURI, String readsURI, Client c) {
		this.name = name;
		this.self = selfURI;
		this.reads = readsURI;
		this.client = c;
		destroyed = false;
	}
	

	@Override /* OK */
	public String getName() throws DestroyedBookshelfException {
		if(destroyed)
			throw new DestroyedBookshelfException();
		return this.name;
	}

	@Override
	public void addItem(ItemReader item)
			throws DestroyedBookshelfException, UnknownItemException, TooManyItemsException, ServiceException {
		
		String id;
		if(item instanceof ItemReaderImpl)
			id = ((ItemReaderImpl) item).getId().toString();
		else
			throw new UnknownItemException();
	
		Response r = client.target(self).request().post(Entity.text(id));
		
		if(r.getStatus() != Status.NO_CONTENT.getStatusCode()) {
			if(r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
				destroyed = true;
				throw new DestroyedBookshelfException();
			}
			if(r.getStatus() == Status.BAD_REQUEST.getStatusCode()){
				throw new UnknownItemException();
			}
			throw new ServiceException();
		}
		
	}

	@Override
	public void removeItem(ItemReader item) throws DestroyedBookshelfException, UnknownItemException, ServiceException {
		
		String id;
		if(item instanceof ItemReaderImpl)
			id = ((ItemReaderImpl) item).getId().toString();
		else
			throw new UnknownItemException();
		
		Response r = client.target(self).path(id).request().delete();
		
		if(r.getStatus() != Status.NO_CONTENT.getStatusCode()) {
			if(r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
				destroyed = true;
				throw new DestroyedBookshelfException();
			}
			if(r.getStatus() == Status.CONFLICT.getStatusCode()){
				throw new UnknownItemException();
			}
			throw new ServiceException();
		}
		
	}

	@Override /* OK */
	public Set<ItemReader> getItems() throws DestroyedBookshelfException, ServiceException {
		Response r = client.target(self).request(MediaType.APPLICATION_JSON_TYPE).get();
		if(r.getStatus() != Status.OK.getStatusCode()) {
			if(r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
				destroyed = true;
				throw new DestroyedBookshelfException();
			}
			throw new ServiceException();
		}
		
		Items items = r.readEntity(Items.class);
		Set<ItemReader> irs = new HashSet<>();
		
		for(Items.Item i : items.getItem()) {
			irs.add(new ItemReaderImpl(i));
		}
		return irs;
	}

	@Override /* OK */
	public void destroyBookshelf() throws DestroyedBookshelfException, ServiceException {
		Response r = client.target(self).request().delete();
		if(r.getStatus() != Status.NO_CONTENT.getStatusCode()) {
			if(r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
				destroyed = true;
				throw new DestroyedBookshelfException();
			}
			throw new ServiceException();
		}
	}

	@Override /* OK */
	public int getNumberOfReads() throws DestroyedBookshelfException, ServiceException {
		Response r = client.target(reads).request(MediaType.TEXT_PLAIN).get();
		if(r.getStatus() != Status.OK.getStatusCode()) {
			if(r.getStatus() == Status.NOT_FOUND.getStatusCode()) {
				destroyed = true;
				throw new DestroyedBookshelfException();
			}
			throw new ServiceException();
		}
		
		BigInteger readsCount = r.readEntity(BigInteger.class);
		return readsCount.intValue();
	}

}
