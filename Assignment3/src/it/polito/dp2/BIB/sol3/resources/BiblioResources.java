package it.polito.dp2.BIB.sol3.resources;

import it.polito.dp2.BIB.sol3.service.jaxb.*;
import it.polito.dp2.BIB.sol3.model.EBiblio;
import it.polito.dp2.BIB.sol3.service.BadRequestServiceException;
import it.polito.dp2.BIB.sol3.service.BiblioService;
import it.polito.dp2.BIB.sol3.service.ConflictServiceException;
import it.polito.dp2.BIB.sol3.service.SearchScope;

import java.math.BigInteger;
import java.net.URI;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/biblio")
@Api(value = "/biblio")
public class BiblioResources {
	public UriInfo uriInfo;
	
	BiblioService service;

	public BiblioResources(@Context UriInfo uriInfo) {
		this.uriInfo = uriInfo;
		this.service = new BiblioService(uriInfo);
	}
	
	@GET
    @ApiOperation(value = "getBiblio", notes = "read main resource"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Biblio.class),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public EBiblio getBiblio() {
		return new EBiblio(uriInfo.getAbsolutePathBuilder());
	}
	
	@GET
	@Path("/items")
    @ApiOperation(value = "getItems", notes = "search items"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Items getItems(
			@ApiParam("The keyword to be used for the search") @QueryParam("keyword") @DefaultValue("") String keyword,
			@ApiParam("The year before which items are searched") @QueryParam("beforeInclusive") @DefaultValue("10000") int beforeInclusive,
			@ApiParam("The year after which items are searched") @QueryParam("afterInclusive") @DefaultValue("0") int afterInclusive,
			@ApiParam("The page of results to be read") @QueryParam("page") @DefaultValue("1") int page
			) {
		try {
			return service.getItems(SearchScope.ALL, keyword, beforeInclusive, afterInclusive, BigInteger.valueOf(page));
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}

	@GET
	@Path("/items/articles")
    @ApiOperation(value = "getArticles", notes = "search articles"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Items getArticles(
			@ApiParam("The keyword to be used for the search") @QueryParam("keyword") String keyword,
			@ApiParam("The year before which items are searched") @QueryParam("beforeInclusive") @DefaultValue("10000") int beforeInclusive,
			@ApiParam("The year after which items are searched") @QueryParam("afterInclusive") @DefaultValue("0") int afterInclusive,
			@ApiParam("The page of results to be read") @QueryParam("page") @DefaultValue("1") int page
			) {
		if (keyword==null)
			throw new BadRequestException("keyword is required");
		try {
			return service.getItems(SearchScope.ARTICLES, keyword, beforeInclusive, afterInclusive, BigInteger.valueOf(page));
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}	
	}

	@GET
	@Path("/items/books")
    @ApiOperation(value = "getBooks", notes = "search books"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Items getBooks(
			@ApiParam("The keyword to be used for the search") @QueryParam("keyword") String keyword,
			@ApiParam("The year before which items are searched") @QueryParam("beforeInclusive") @DefaultValue("10000") int beforeInclusive,
			@ApiParam("The year after which items are searched") @QueryParam("afterInclusive") @DefaultValue("0") int afterInclusive,
			@ApiParam("The page of results to be read") @QueryParam("page") @DefaultValue("1") int page
			) {
		if (keyword==null)
			throw new BadRequestException("keyword is required");
		try {
			return service.getItems(SearchScope.BOOKS, keyword, beforeInclusive, afterInclusive, BigInteger.valueOf(page));
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}

	}

	@POST
	@Path("/items")
    @ApiOperation(value = "createItem", notes = "create a new item", response=Item.class
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 201, message = "OK", response=Item.class),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response createItem(Item item) {
		try {
			Item returnItem = service.createItem(item);
			return Response.created(new URI(returnItem.getSelf())).entity(returnItem).build();
		} catch (Exception e1) {
			throw new InternalServerErrorException();
		}
	}

	@GET
	@Path("/items/{id}")
    @ApiOperation(value = "getItem", notes = "read a single item"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Item.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Item getItem(
			@ApiParam("The id of the item") @PathParam("id") BigInteger id) {
		Item item;
		try {
			item = service.getItem(id);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (item==null)
			throw new NotFoundException();
		return item;
	}
	
	@PUT
	@Path("/items/{id}")
    @ApiOperation(value = "updateItem", notes = "update a single item"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Item.class),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Item updateItem(
			@ApiParam("The id of the item") @PathParam("id") BigInteger id,
			Item item) {
		Item updated;
		try {
			updated = service.updateItem(id, item);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (updated==null)
			throw new NotFoundException();
		return updated;
	}
	
	@DELETE
	@Path("/items/{id}")
    @ApiOperation(value = "deleteItem", notes = "delete a single item"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 204, message = "No content"),
    		@ApiResponse(code = 404, message = "Not Found"),
    		@ApiResponse(code = 409, message = "Conflict (item is cited)"),
    		})
	public void deleteItem(
			@ApiParam("The id of the item") @PathParam("id") BigInteger id) {
		BigInteger ret;
		try {
			ret = service.deleteItem(id);
		} catch (ConflictServiceException e) {
			throw new ClientErrorException(409);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (ret==null)
			throw new NotFoundException();
		return;
	}
	
	@GET
	@Path("/items/{id}/citedBy")
    @ApiOperation(value = "getItemCitedBy", notes = "read the items citing an item"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Items getItemCitedBy(
			@ApiParam("The id of the item for which citing items have to be read") @PathParam("id") BigInteger id) {
		Items items;
		try {
			items = service.getItemCitedBy(id);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (items==null)
			throw new NotFoundException();
		return items;
	}

	@GET
	@Path("/items/{id}/citations/targets")
    @ApiOperation(value = "getItemCitations", notes = "read the target items of the citations from an item"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Items getItemCitations(
			@ApiParam("The id of the item from which citations are considered") @PathParam("id") BigInteger id) {
		Items items;
		try {
			items = service.getItemCitations(id);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (items==null)
			throw new NotFoundException();
		return items;
	}

	@GET
	@Path("/items/{id}/citations/{tid}")
    @ApiOperation(value = "getItemCitation", notes = "read a citation"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Citation.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Citation getItemCitation(
			@ApiParam("The id of the citing item of this citation") @PathParam("id") BigInteger id,
			@ApiParam("The id of the cited item of this citation") @PathParam("tid") BigInteger tid) throws Exception {
		Citation citation;
		try {
			citation = service.getItemCitation(id,tid);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if (citation==null)
			throw new NotFoundException();
		return citation;
	}

	@PUT
	@Path("/items/{id}/citations/{tid}")
    @ApiOperation(value = "createItemCitation", notes = "create a citation", response=Citation.class
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 201, message = "Created", response=Citation.class),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		@ApiResponse(code = 404, message = "Not Found"),
    		@ApiResponse(code = 409, message = "Conflict"),
    		})
	@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response createItemCitation(
			@ApiParam("The id of the citing item of this citation") @PathParam("id") BigInteger id,
			@ApiParam("The id of the cited item of this citation") @PathParam("tid") BigInteger tid,
			Citation citation) throws Exception {
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		System.out.println(uriInfo.getBaseUri());
    	URI u = builder.build();
    	UriBuilder fromBuilder = UriBuilder.fromUri(citation.getFrom());
    	URI u2 = fromBuilder.path("citations").path(tid.toString()).build();
	    if (!u.equals(u2))
	    	throw new BadRequestException();
	    citation.setSelf(u.toString());
	    Citation newCitation;
	    try {
	    	newCitation = service.createItemCitation(id, tid, citation);
	    } catch (BadRequestServiceException e) {
			throw new BadRequestException();
		} catch (ConflictServiceException e) {
			throw new ClientErrorException(409);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}	
		if (newCitation==null)
			throw new NotFoundException();
	    return Response.created(u).entity(newCitation).build();
	    	
	}
	
	@DELETE
	@Path("/items/{id}/citations/{tid}")
    @ApiOperation(value = "deleteItemCitation", notes = "delete a citation"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 204, message = "OK", response=Citation.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	public void deleteItemCitation(
			@ApiParam("The id of the citing item of this citation") @PathParam("id") BigInteger id, 
			@ApiParam("The id of the cited item of this citation") @PathParam("tid") BigInteger tid) {
		boolean success;
		try {
			success=service.deleteItemCitation(id,tid);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if(!success)
			throw new NotFoundException();
		return;
	}
	
/* ####################################################################################################### */
	
	/* Create Bookshelf */
	@POST
	@Path("/bookshelves")
    @ApiOperation(value = "createBookshelf", notes = "create a new bookshelf", response=Bookshelf.class)
    @ApiResponses(value = {
    		@ApiResponse(code = 201, message = "OK", response=Bookshelf.class),
    		@ApiResponse(code = 400, message = "Bad Request"),
    		})
	@Consumes({MediaType.TEXT_PLAIN})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Bookshelf createBookshelf(String name) {
		
		try {
			return service.createBookshelf(name);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
	}
	
	/* Search Bookshelves */
	@GET
	@Path("/bookshelves")
    @ApiOperation(value = "getBookshelves", notes = "search bookshelves with any name match")
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Bookshelves.class),
    		})
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Bookshelves getBookshelves(
			@ApiParam("The keyword to be used for the search") @QueryParam("name") @DefaultValue("") String name
			) {
		try {
			return service.getBookshelves(name);
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/* Get Items from a Bookshelf */
	@GET
	@Path("/bookshelves/{id}")
	@ApiOperation(value = "getItemsFromBookshelf", notes = "get items from a bookshelf")
	@ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=Items.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Items getItemsFromBookshelf(@ApiParam("The id of the desired bookshelf") @PathParam("id") BigInteger id) {
		try {
			return service.getItemsFromBookshelf(id);
		} catch (BadRequestServiceException e) {
			throw new NotFoundException();
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
	/* Delete Bookshelf */
	@DELETE
	@Path("/bookshelves/{id}")
    @ApiOperation(value = "deleteBookshelf", notes = "delete a whole bookshelf")
    @ApiResponses(value = {
    		@ApiResponse(code = 204, message = "OK", response=BigInteger.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	public void deleteBookshelf(
			@ApiParam("The id of the desired bookshelf") @PathParam("id") BigInteger id) {
		boolean success;
		try {
			success = service.deleteBookshelf(id);
		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
		if(!success)
			throw new NotFoundException();
		
	}
	
	/* Add Item to Bookshelf */
	@POST
	@Path("/bookshelves/{id}")
    @ApiOperation(value = "addItemToBookshelf", notes = "add an item to the bookshelf")
    @ApiResponses(value = {
    		@ApiResponse(code = 201, message = "Created", response=BigInteger.class),
    		@ApiResponse(code = 400, message = "Bad Request - Item doesn't exist anymore"),
    		@ApiResponse(code = 404, message = "Not Found - Bookshelf doesn't exist anymore"),
    		@ApiResponse(code = 409, message = "Conflict - Too many items"),
    		})
	@Consumes({MediaType.TEXT_PLAIN})
	public void addItemToBookshelf(
			@ApiParam("The id of desired bookshelf") @PathParam("id") BigInteger id, BigInteger iid) {
		int r;
		
		try {
			r = service.addItemToBookshelf(id, iid).intValue();
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(); // ItemNotFound
		} catch (Exception e) {
			throw new InternalServerErrorException(e); // ServiceError
		}
		
		switch (r) {
			case -1: throw new NotFoundException(); // BookshelfNotFound
			case -2: throw new ClientErrorException(409); // TooManyItems
		}
	    	
	}
	
	/* Remove Item from Bookshelf */
	@DELETE
	@Path("/bookshelves/{id}/{iid}")
    @ApiOperation(value = "removeItemFromBookshelf", notes = "remove an item from the bookshelf"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 204, message = "OK", response=BigInteger.class),
    		@ApiResponse(code = 404, message = "Not Found")
    		})
	public void removeItemFromBookshelf(
			@ApiParam("The id of desired bookshelf") @PathParam("id") BigInteger id,
			@ApiParam("The id of the item to remove from the bookshelf") @PathParam("iid") BigInteger iid) throws Exception {
		BigInteger r;
		try {
			r = service.removeItemFromBookshelf(id, iid);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException(); // ItemNotFound
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		
		if(r.equals(BigInteger.valueOf(-1)))
			throw new  NotFoundException(); // BookshelfNotFound
	}
	
	/* Get Reads Number */
	@GET
	@Path("/bookshelves/{id}/reads")
    @ApiOperation(value = "getReadsNumber", notes = "read the number of reads for a bookshelf"
	)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "OK", response=BigInteger.class),
    		@ApiResponse(code = 404, message = "Not Found"),
    		})
	@Produces({MediaType.TEXT_PLAIN})
	public BigInteger getReadsNumber(
			@ApiParam("The id of desired bookshelf") @PathParam("id") BigInteger id) throws Exception {
		BigInteger readsCount;
		try {
			readsCount = service.getReadsNumber(id);
		} catch (Exception e) {
			throw new InternalServerErrorException();
		}
		if(readsCount.equals(BigInteger.valueOf(-1))) {
			throw new NotFoundException();
		}
		return readsCount;
	}
	
}
