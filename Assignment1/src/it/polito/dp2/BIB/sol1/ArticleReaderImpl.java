package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.TreeMap;

import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;

public class ArticleReaderImpl extends ItemReaderImpl implements it.polito.dp2.BIB.ArticleReader {
	
	private ArticleType article;
	private JournalReaderImpl journal;
	private IssueReaderImpl issue;
	
	public ArticleReaderImpl(BiblioItemType item, 
			TreeMap<BigInteger, ItemReaderImpl> items, 
			TreeMap<String, JournalReaderImpl> journals,
			TreeMap<BigInteger, IssueReaderImpl> issues) {
		super(item, items);
		this.article = (ArticleType) item;
		this.journal = journals.get(this.article.getJournal());
		this.issue = issues.get(article.getIssue());
		this.issue.addArticle(this);
	}

	@Override
	public IssueReader getIssue() {
		return (IssueReader)this.issue;
	}

	@Override
	public JournalReader getJournal() {
		return (JournalReader)this.journal;
	}

	@Override
	public int getYear() {
		return this.issue.getYear();
	}

}
