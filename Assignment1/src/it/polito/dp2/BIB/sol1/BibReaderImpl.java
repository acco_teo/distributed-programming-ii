package it.polito.dp2.BIB.sol1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import it.polito.dp2.BIB.BibReader;
import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;

public class BibReaderImpl implements BibReader {
	
	private TreeMap<BigInteger, ItemReaderImpl> items; 
	private TreeMap<String, JournalReaderImpl> journals;
	private TreeMap<BigInteger, IssueReaderImpl> issues;
	private String schema = "xsd/biblio_e.xsd";
	
	BibReaderImpl() throws Exception {
		items = new TreeMap<>();
		journals = new TreeMap<>();
		issues = new TreeMap<>();
		String xmlFile = System.getProperty("it.polito.dp2.BIB.sol1.BibInfo.file", "xsd/biblio.xml");

		System.out.println("Reference BibReader: Using file: " + xmlFile);
		JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.BIB.sol1.jaxb");
		Unmarshaller u = jc.createUnmarshaller();
		
		try {
			SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			u.setSchema(sf.newSchema(new File(this.schema)));
			Biblio biblio = (Biblio) u.unmarshal(new FileInputStream(xmlFile));
			
			/* Populating Journals and Issues Maps */
			biblio.getJournal().forEach(j -> journals.put(j.getISSN(), new JournalReaderImpl(j, issues)));
			
			/* Populating Items Map with Books */
			biblio.getBook().forEach(b -> 
				this.items.put(b.getId(), new BookReaderImpl(b, this.items)));
			
			/* Populating Items Map with Articles */
			biblio.getArticle().forEach(a -> 
				this.items.put(a.getId(), new ArticleReaderImpl(a, this.items, this.journals, this.issues)));
			
		} catch (SAXException se) {
			System.err.println("Could not use schema " + this.schema);
			throw se;
		} catch (FileNotFoundException fe) {
			System.err.println("Could not use schema " + this.schema);
			throw fe;
		}
	}

	@Override
	public BookReader getBook(String isbn) {
		return (BookReader) items.values().stream()
				.filter(t1 -> t1 instanceof BookReaderImpl)
				.filter(t2 -> ((BookReaderImpl) t2).getISBN().equals(isbn))
				.limit(1);
	}

	@Override
	public Set<ItemReader> getItems(String keyword, int since, int to) {
		if(keyword == null) {
			/* null keyword -> return journals with just since-to filter applied */
			return items.values().stream()
					.filter(t -> t.getYear()>=since && t.getYear()<=to)
					.collect(Collectors.toSet());
		}
		else {
			/* !null keyword -> return journals with keyword match and since-to filter applied */
			return items.values().stream()
					.filter(t -> t.getTitle().indexOf(keyword)>=0 && t.getYear()>=since && t.getYear()<=to)
					.collect(Collectors.toSet());
		}
	}

	@Override
	public JournalReader getJournal(String issn) {
		return journals.get(issn);
	}

	@Override
	public Set<JournalReader> getJournals(String keyword) {
		if(keyword == null) {
			/* null keyword -> return all journals */
			return journals.values()
					.stream()
					.collect(Collectors.toSet());
		}
		else {
			/* !null keyword -> return journals with keyword match */
			return journals.values()
					.stream()
					.filter(t -> t.getTitle().indexOf(keyword)>=0)
					.collect(Collectors.toSet());
		}
	}

}