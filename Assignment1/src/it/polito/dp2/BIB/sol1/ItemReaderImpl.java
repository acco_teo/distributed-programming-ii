package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;

public abstract class ItemReaderImpl implements ItemReader {
	
	BiblioItemType item;
	private TreeMap<BigInteger, ItemReaderImpl> items;
	
	ItemReaderImpl(BiblioItemType item, TreeMap<BigInteger, ItemReaderImpl> items) {
		this.item = item;
		this.items = items;
	}

	@Override
	public String[] getAuthors() {
		return this.item.getAuthor().toArray(new String[0]);
	}

	@Override
	public Set<ItemReader> getCitingItems() {
		Set<ItemReader> returnSet = new HashSet<>();
		this.item.getCitedBy().forEach(id -> returnSet.add(this.items.get(id)));
		return returnSet;
	}

	@Override
	public String getSubtitle() {
		return this.item.getSubtitle();
	}

	@Override
	public String getTitle() {
		return this.item.getTitle();
	}
	
	public abstract int getYear();

}
