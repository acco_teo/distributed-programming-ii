package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;


public class JournalReaderImpl implements JournalReader {
	
	private JournalType journal;
	private TreeMap<BigInteger, IssueReaderImpl> issues;
	
	JournalReaderImpl(JournalType journal, TreeMap<BigInteger, IssueReaderImpl> globalIssues) {
		issues = new TreeMap<>();
		this.journal = journal;
		journal.getIssue().forEach(i -> this.issues.put(i.getId(), new IssueReaderImpl(i, this)));
		globalIssues.putAll(issues);
	}

	@Override
	public String getISSN() {
		return this.journal.getISSN();
	}

	@Override
	public IssueReader getIssue(int year, int number) {
		return (IssueReader) issues.values().stream()
				.filter(t -> t.getNumber()==number && t.getYear()==year)
				.limit(1);
	}

	@Override
	public Set<IssueReader> getIssues(int since, int to) {
		return issues.values().stream()
				.filter(t -> t.getYear() >= since && t.getYear() <= to)
				.collect(Collectors.toSet());
	}

	@Override
	public String getPublisher() {
		return this.journal.getPublisher();
	}

	@Override
	public String getTitle() {
		return this.journal.getTitle();
	}

}
