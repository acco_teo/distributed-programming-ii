package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.TreeMap;

import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;
import it.polito.dp2.BIB.sol1.jaxb.BookType;

public class BookReaderImpl extends ItemReaderImpl implements BookReader {
	
	BookType book;
	
	BookReaderImpl(BiblioItemType item, TreeMap<BigInteger, ItemReaderImpl> items) {
		super(item, items);
		this.book = (BookType) item;
	}

	@Override
	public String getISBN() {
		return this.book.getISBN();
	}

	@Override
	public String getPublisher() {
		return this.book.getPublisher();
	}

	@Override
	public int getYear() {
		return this.book.getYear().getYear();
	}

}
