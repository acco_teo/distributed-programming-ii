package it.polito.dp2.BIB.sol1;

import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.ArticleReader;
import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;

public class IssueReaderImpl implements IssueReader {
	JournalType.Issue issue;
	JournalReader journal;
	Set<ArticleReader> articles;

	IssueReaderImpl(JournalType.Issue issue, JournalReader journal) {
		this.issue = issue;
		this.journal = journal;
		this.articles = new HashSet<>();
	}

	@Override
	public Set<ArticleReader> getArticles() {
		return this.articles;
	}

	@Override
	public JournalReader getJournal() {
		return this.journal;
	}

	@Override
	public int getNumber() {
		return this.issue.getNumber().intValue();
	}

	@Override
	public int getYear() {
		return this.issue.getYear().getYear();
	}

	public void addArticle(ArticleReaderImpl article) {
		this.articles.add(article);
	}

}
