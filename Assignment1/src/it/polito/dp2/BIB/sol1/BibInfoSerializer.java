package it.polito.dp2.BIB.sol1;

import java.io.File;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import it.polito.dp2.BIB.ArticleReader;
import it.polito.dp2.BIB.BibReader;
import it.polito.dp2.BIB.BibReaderException;
import it.polito.dp2.BIB.BibReaderFactory;
import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.FactoryConfigurationError;
import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.BookType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;
import it.polito.dp2.BIB.sol1.jaxb.ObjectFactory;

public class BibInfoSerializer {
	
	private BibReader monitor;
	private ObjectFactory of;
	
	static final int INPUT_FILE_ERROR	= 1;
	static final int SERIALIZER_ERROR	= 2;
	static final int MARSHAL_ERROR		= 3;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		BibInfoSerializer bis = null;
		JAXBContext jc;
		Marshaller m;
		Biblio b = new Biblio();
		
		if(args.length != 1) {
			System.err.println("Please, insert an XML output file as a parameter");
			System.exit(INPUT_FILE_ERROR);
		}
		
		try {
			bis = new BibInfoSerializer();
			
			/* Inserting data into structures */
			b.getArticle().addAll(bis.getArticles());
			b.getBook().addAll(bis.getBooks());
			b.getJournal().addAll(bis.getJournals());
			
		} catch(BibReaderException e) {
			System.err.println("Could not instantiate data generator.");
			e.printStackTrace();
			System.exit(SERIALIZER_ERROR);
		}
		
		/* Marshaling */
		try {
			jc = JAXBContext.newInstance("it.polito.dp2.BIB.sol1.jaxb");
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(b, new File(args[0]));
		} catch(JAXBException e) {
			System.err.println("Error during marshal operations.");
			e.printStackTrace();
			System.exit(MARSHAL_ERROR);
		}	
	}

	/**
	 * Default constructor
	 * @throws BibReaderException 
	 */
	public BibInfoSerializer() throws BibReaderException {
		BibReaderFactory factory;
		
		try {
			factory = BibReaderFactory.newInstance();
		} catch(FactoryConfigurationError fce) {
			throw new BibReaderException(fce.getMessage());
		}
		
		monitor = factory.newBibReader();
		of = new ObjectFactory();
	}

	public BibInfoSerializer(BibReader monitor) {
		super();
		this.monitor = monitor;
	}
	
	public Set<JournalType> getJournals() {
		Set<JournalReader> reader = monitor.getJournals(null);
		Set<JournalType> journals = new HashSet<>();
		
		for(JournalReader jr : reader) {
			JournalType j = of.createJournalType(); 
			j.setISSN(jr.getISSN());
			j.setPublisher(jr.getPublisher());
			j.setTitle(jr.getTitle());
			setIssues(j, jr);
			journals.add(j);
		}
		
		return journals;
	}
	
	private void setIssues(JournalType j, JournalReader jr) {
		for(IssueReader ir : jr.getIssues(0, 3000)) {
			Issue i = of.createJournalTypeIssue();
			i.setId(BigInteger.valueOf(ir.hashCode())); 
			i.setNumber(BigInteger.valueOf(ir.getNumber()));
			 try {
				XMLGregorianCalendar d = DatatypeFactory.newInstance().newXMLGregorianCalendar();
				d.clear();
				d.setYear(ir.getYear());
				i.setYear(d);
			} catch (DatatypeConfigurationException e) {
				System.err.println("Invalid XMLGregorianCalendar - Journal");
				e.printStackTrace();
				System.exit(SERIALIZER_ERROR);
			}
			j.getIssue().add(i);
		}
	}
	
	public Set<ArticleType> getArticles() {
		Set<ItemReader> reader = monitor.getItems(null, 0, 3000);
		Set<ArticleType> articles = new HashSet<>();
		for(ItemReader ar : reader) {
			if(ar instanceof ArticleReader) {
				ArticleType a = of.createArticleType();
				a.setId(BigInteger.valueOf(ar.hashCode()));
				a.setTitle(ar.getTitle());
				a.setSubtitle(ar.getSubtitle());
				a.setJournal(((ArticleReader) ar).getJournal().getISSN());
				a.setIssue(BigInteger.valueOf(((ArticleReader) ar).getIssue().hashCode()));
				for(String author : ar.getAuthors())
					a.getAuthor().add(author);
				for(ItemReader ir : ar.getCitingItems())
					a.getCitedBy().add(BigInteger.valueOf(ir.hashCode()));
				articles.add(a);
			}
		}
		return articles;
	}
	
	public Set<BookType> getBooks() {
		Set<ItemReader> reader = monitor.getItems(null, 0, 3000);
		Set<BookType> books = new HashSet<>();
		for(ItemReader br : reader) {
			if(br instanceof BookReader) {
				BookType b = of.createBookType();
				b.setId(BigInteger.valueOf(br.hashCode()));
				b.setTitle(br.getTitle());
				b.setSubtitle(br.getSubtitle());
				for(String author : br.getAuthors())
					b.getAuthor().add(author);
				for(ItemReader ir : br.getCitingItems())
					b.getCitedBy().add(BigInteger.valueOf(ir.hashCode()));
				b.setISBN(((BookReader) br).getISBN());
				b.setPublisher(((BookReader) br).getPublisher());
				try {
					XMLGregorianCalendar d = DatatypeFactory.newInstance().newXMLGregorianCalendar();
					d.clear();
					d.setYear(((BookReader) br).getYear());
					b.setYear(d);
				} catch (DatatypeConfigurationException e) {
					System.err.println("Invalid XMLGregorianCalendar - Book");
					e.printStackTrace();
					System.exit(SERIALIZER_ERROR);
				}
				books.add(b);
			}
		}
		return books;
	}
	
	
	/* ################################################################################################################ */
	public void printAll() {
		printLine(' ');
		System.out.println("Bibliography");
		System.out.println("Bibliography items:");
		printLine(' ');

		printItems();
		printJournals();

	}

	private void printItems() {
		// Get the list of Items
		Set<ItemReader> set = monitor.getItems(null, 0, 3000);

		/* Print the header of the table */
		printHeader('#', "#Information about ITEMS");
		printHeader("#Number of Items: " + set.size());
		printHeader("#List of Items:");
		printLine('-');

		// For each Item print related data
		for (ItemReader item : set) {
			System.out.println("Title: " + item.getTitle());
			if (item.getSubtitle() != null)
				System.out.println("Subtitle: " + item.getSubtitle());
			System.out.print("Authors: ");
			String[] authors = item.getAuthors();
			System.out.print(authors[0]);
			for (int i = 1; i < authors.length; i++)
				System.out.print(", " + authors[i]);
			System.out.println(";");
			if (item instanceof ArticleReader) {
				ArticleReader article = (ArticleReader) item;
				System.out.println("Article in Journal " + article.getJournal().getTitle() + "; Issue "
						+ article.getIssue().getNumber() + "," + article.getIssue().getYear());
			}
			if (item instanceof BookReader) {
				BookReader book = (BookReader) item;
				System.out.print("Book ");
				System.out.print("published by " + book.getPublisher());
				System.out.print(" in " + book.getYear());
				System.out.println(" (ISBN " + book.getISBN() + ")");
			}
			Set<ItemReader> citingItems = item.getCitingItems();
			System.out.println("Cited by " + citingItems.size() + " items:");
			for (ItemReader citing : citingItems) {
				System.out.println("- " + citing.getTitle());
			}
			printLine('-');

		}
		printBlankLine();
	}

	private void printJournals() {
		// Get the list of journals
		Set<JournalReader> set = monitor.getJournals(null);
		/* Print the header of the table */
		printHeader('#', "#Information about JOURNALS");
		printHeader("#Number of Journals: " + set.size());
		printHeader("#List of Journals:");
		printLine('-');

		for (JournalReader journal : set) {
			System.out.println("Title: " + journal.getTitle());
			System.out.println("Publisher: " + journal.getPublisher());
			System.out.println("ISSN: " + journal.getISSN());
			System.out.println("Issues:");
			for (IssueReader issue : journal.getIssues(0, 3000)) {
				System.out.println("Year: " + issue.getYear() + ";" + " Number: " + issue.getNumber());
			}
			printLine('-');
		}

	}

	private void printBlankLine() {
		System.out.println(" ");
	}

	private void printLine(char c) {
		System.out.println(makeLine(c));
	}

	private void printHeader(String header) {
		System.out.println(header);
	}

	private void printHeader(String header, char c) {
		System.out.println(header);
		printLine(c);
	}

	private void printHeader(char c, String header) {
		printLine(c);
		System.out.println(header);
	}

	private StringBuffer makeLine(char c) {
		StringBuffer line = new StringBuffer(132);

		for (int i = 0; i < 132; ++i) {
			line.append(c);
		}
		return line;
	}

}
